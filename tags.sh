#!/bin/sh

cd $PWD
PRUNE_DIR=
if [ -z $1 ]
then
PRUNE_DIR=.git
else
PRUNE_DIR=$1 
fi
echo PRUNE_DIR=$PRUNE_DIR

for i in `find . -name "$PRUNE_DIR" -prune -o -type d`

do
cd $i
ctags -R .
cd - 2>&1 >/dev/null
done
