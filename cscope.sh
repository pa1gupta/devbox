#!/bin/sh
#Run this from top level source dir
#
# """"""""""""" My cscope/vim key mappings Ctrl-\
#"
#" The following maps all invoke one of the following cscope search types:
#  "
#  "   's'   symbol: find all references to the token under cursor
#  "   'g'   global: find global definition(s) of the token under cursor
#  "   'c'   calls:  find all calls to the function name under cursor
#  "   't'   text:   find all instances of the text under cursor
#  "   'e'   egrep:  egrep search for the word under cursor
#  "   'f'   file:   open the filename under cursor
#  "   'i'   includes: find files that include the filename under cursor
#  "   'd'   called: find functions that function under cursor calls
#

find $PWD -type f -name "*.c" -o -name "*.cpp" -o -name "*.h" -o -name "*.S" > cscope.files
cscope -q -R -b -i cscope.files
